use rand::Rng;
use std::cmp::Ordering;
use std::io;

use colored::Colorize;

fn main() {
    guess_number();
}

fn guess_number() {
    println!("== Guess the number! ==");
    println!("Min = 0; Max = 255\n");
    let mut iterator: u16 = 0;
    let secret_number: u8 = rand::thread_rng().gen_range(u8::MIN..=u8::MAX);

    loop {
        if iterator == 0 {
            println!("{}", "Please input your guess: ".bright_blue());
        } else {
            println!("{}", "Please input your new guess: ".bright_blue());
        }

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read input :(");

        let guess: u8 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("{}", "Too small.".bright_green()),
            Ordering::Greater => println!("{}", "Too big.".bright_red()),
            Ordering::Equal => {
                if iterator == 0 {
                    println!("{}", "\nWOW! You won on the first try!".bright_purple());
                    break;
                }
                
                println!("\nYou win :D ({} attempts before win)", iterator);
                break;
            }
        }

        iterator += 1;
    }
}
